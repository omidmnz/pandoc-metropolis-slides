# Markdown Presentation with Metropolis Theme
Run the following:
```sh
make presentation.pdf
```
or
```sh
make
```

If you need more arguments set for Pandoc, you can set them as follows before running make:
```sh
export PANDOC_FLAGS="-F pandoc-crossref -F pandoc-citeproc"
```

or for showing bullet points incrementally:
```sh
export PANDOC_FLAGS="-i"
```
