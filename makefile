PANDOC_CMD = pandoc -s -t beamer
.POSIX:
all: presentation.pdf

presentation.pdf: presentation.md

presentation.tex: presentation.md

.PHONY:
clean:
	rm -rf presentation.pdf presentation.tex presentation.aux presentation.log presentation.nav presentation.snm presentation.toc presentation.vrb

.SUFFIXES: .md .pdf .tex
.md.pdf:
	$(PANDOC_CMD) $(PANDOC_FLAGS) $< -o $@

.md.tex:
	$(PANDOC_CMD) $(PANDOC_FLAGS) $< -o $@
