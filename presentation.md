---
title: Presentation Title
subtitle: Presentation Subtitle
author: Author Name
header-includes:
  - \usetheme[progressbar=frametitle,sectionpage=progressbar,numbering=fraction,background=light,block=fill]{metropolis} 
  - \usefonttheme[onlymath]{serif}
---

# Section
## Slide Title
Some content here...

### Quote
This should be a quote

---

this should be a header-less slide

# Special Blocks
## Source code
```py
import matplotlib.pyplot as plt
import numpy as np

x = np.linspace(0, np.pi, 10000)
plt.plot(x, np.sin(1 / x))
plt.savefig("./images/sample_image.pdf")
```

## An image
![this is the caption](./images/sample_image.pdf){#fig:devil width=70%}

## References
[@fig:devil] should reference the previous slide's image, if
`pandoc-crossref` is used.

## Math
Math is handled with \(\LaTeX\):

$$ e^{i\pi} + 1 = 0 $$

## Bullet Points
- First
- Second
- Third
- Fourth
